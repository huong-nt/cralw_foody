#encoding : UTF-8
require_relative "../../initialize.rb"
 
require_relative ROOT_DIRECTORY + "crawl/CrawlFoody.rb"

def runCrawlFoody(city, action)
  crawler = CrawlFoody.new(city)
  crawler.createTable
  if action == 'detail'
  	crawler.getDetail
  else
    crawler.getLink
  end
end


city = ARGV[0].dup.force_encoding("UTF-8");
source = ARGV[1].dup.force_encoding("UTF-8");
action = ARGV[2].dup.force_encoding("UTF-8");
#arg1 = ARGV[3].dup.force_encoding("UTF-8");
city = Crawl.getShortNameOfCity(city)


if source == "CrawlFoody"
  runCrawlFoody(city, action)
end

