#encoding : UTF-8
class MyString
    def self.stripString(string)
        if string == nil
            return ""
        end
        return string.strip.gsub(/\s+/," ")
    end
    def self.boDauVaChuThuong(title)
        title2 = title.to_s
        title2 = title2.gsub(/ấ|ầ|ẩ|ẫ|ậ|Ấ|Ầ|Ẩ|Ẫ|Ậ|ắ|ằ|ẳ|ẵ|ặ|Ắ|Ằ|Ẳ|Ẵ|Ặ|á|à|ả|ã|ạ|â|ă|Á|À|Ả|Ã|Ạ|Â|Ă/,'a')
        title2 = title2.gsub(/ế|ề|ể|ễ|ệ|Ế|Ề|Ể|Ễ|Ệ|é|è|ẻ|ẽ|ẹ|ê|É|È|Ẻ|Ẽ|Ẹ|Ê/,'e')
        title2 = title2.gsub(/í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị/,'i')
        title2 = title2.gsub(/ố|ồ|ổ|ỗ|ộ|Ố|Ồ|Ổ|Ô|Ộ|ớ|ờ|ở|ỡ|ợ|Ớ|Ờ|Ở|Ỡ|Ợ|ó|ò|ỏ|õ|ọ|ô|ơ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ơ/,'o')
        title2 = title2.gsub(/ứ|ừ|ử|ữ|ự|Ứ|Ừ|Ử|Ữ|Ự|ú|ù|ủ|ũ|ụ|ư|Ú|Ù|Ủ|Ũ|Ụ|Ư/,'u')
        title2 = title2.gsub(/ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ/,'y')
        title2 = title2.gsub(/đ|Đ/,'d')
        return title2.strip.downcase
    end
    def self.boDau(title)
        title2 = title.to_s
        title2 = title2.gsub(/ấ|ầ|ẩ|ẫ|ậ|ắ|ằ|ẳ|ẵ|ặ|á|à|ả|ã|ạ|â|ă/,'a')
        title2 = title2.gsub(/Ấ|Ầ|Ẩ|Ẫ|Ậ|Á|À|Ả|Ã|Ạ|Â|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ/,'A')
        title2 = title2.gsub(/ế|ề|ể|ễ|ệ|é|è|ẻ|ẽ|ẹ|ê/,'e')
        title2 = title2.gsub(/Ế|Ề|Ể|Ễ|Ệ|É|È|Ẻ|Ẽ|Ẹ|Ê/,'E')
        title2 = title2.gsub(/í|ì|ỉ|ĩ|ị/,'i')
        title2 = title2.gsub(/Í|Ì|Ỉ|Ĩ|Ị/,'I')
        title2 = title2.gsub(/ố|ồ|ổ|ỗ|ộ|ớ|ờ|ở|ỡ|ợ|ó|ò|ỏ|õ|ọ|ô|ơ/,'o')
        title2 = title2.gsub(/Ố|Ồ|Ổ|Ỗ|Ộ|Ớ|Ờ|Ở|Ỡ|Ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ơ/,'O')
        title2 = title2.gsub(/ứ|ừ|ử|ữ|ự|ú|ù|ủ|ũ|ụ|ư/,'u')
        title2 = title2.gsub(/Ứ|Ừ|Ử|Ữ|Ự|Ú|Ù|Ủ|Ũ|Ụ|Ư/,'U')
        title2 = title2.gsub(/ý|ỳ|ỷ|ỹ|ỵ/,'y')
        title2 = title2.gsub(/Ý|Ỳ|Ỷ|Ỹ|Ỵ/,'Y')
        title2 = title2.gsub(/đ/,'d')
        title2 = title2.gsub(/Đ/,'D')
        return title2
    end
    def self.myDownCase(string)
        string = string.downcase.gsub(/Đ/,'đ').gsub(/Ấ/, 'ấ').gsub(/Á/, 'á').gsub(/Ố/, "ố")
        return string
    end
    def self.gsubArray(array, string)
        array.map { |e|
            string = string.gsub(e, " ")
            string = string.gsub(/\s+/, " ").strip
        }
        return string
    end
    def self.gsubArrayUseChuThuong(array, stringBandau)
        string = myDownCase(stringBandau)
        array.map { |e|
            e = myDownCase(e)
            string = string.gsub(e, " ")
            string = string.gsub(/\s+/, " ").strip
        }
        return string
        #return getStringCoDau(stringBandau, string)
    end
    def self.gsubArrayAtBegin(array, string)
        array.map { |e|
            if string.index(e)==0
                string = string.gsub(e, " ")
            end
            string = string.gsub(/\s+/, " ").strip
        }
        return string
    end
    def self.gsubArrayAtBeginUseChuThuong(array, stringBanDau)
        stringBanDau = stripString(stringBanDau)
        string = myDownCase(stringBanDau)
        array.map { |e|
            e = myDownCase(e)
            if string.index(e)==0
                string = string.gsub(e, " ")
            end
            string = string.gsub(/\s+/, " ").strip
        }
        return string
        # return getStringCoDau(stringBanDau, string)
    end
    def self.getStringCoDau(stringTo, stringBe)
        if stringBe == nil or stringBe == ''
            return ''
        end
        #puts "string to : #{stringTo}, string be:#{stringBe}"
        temp = boDauVaChuThuong(stringTo)
        stringBe = boDauVaChuThuong(stringBe)
        #puts temp
        indexDau = temp.index(stringBe)
        if indexDau == nil
            return ''
        end
        #puts indexDau
        indexCuoi = indexDau + stringBe.length
        #puts indexCuoi
        if indexCuoi == indexDau
            return ""
        end
        return stringTo[indexDau..indexCuoi-1]
    end
    def self.escStr(str, con)
        if str==nil
            return ""
        end
        return con.escape_string(str).force_encoding("UTF-8")
    end
    def self.gsubLast(strTo1, strBe1)
        strTo = String.new(strTo1)
        strBe = String.new(strBe1)

        i = strTo.rindex(strBe)
        if i==nil
            return strTo
        end
        strTo[i..i+strBe.size-1] = ""
        return strTo
    end
    def self.gsubFirst(strTo1, strBe1)
        strTo = String.new(strTo1)
        strBe = String.new(strBe1)

        i = strTo.index(strBe)
        if i==nil
            return strTo
        end
        strTo[i..i+strBe.size-1] = ""
        return strTo
    end
    def self.findMatch(str1, c)
        a = Array.new
        for i in 0..str1.length-1 
            if str1[i]==c
                a.push(i)
            end
        end
        return a
    end
    def self.getPhanChung(s1, s2)
        l1 = s1.length
        l2 = s2.length
        beginIndex = 0
        thungChua = ""
        maxThungChua = ""
        while true
            match_r = findMatch(s2, s1[beginIndex])
            if match_r.size > 0 
                for i in 0..match_r.size-1
                    indexS2 = match_r[i]
                    indexS1 = beginIndex
                    while s2[indexS2] == s1[indexS1]
                        thungChua = thungChua + s2[indexS2]
                        indexS2  = indexS2 + 1
                        indexS1 = indexS1 + 1

                        if indexS2 == l2 or indexS1 == l1
                            break
                        end
                    end

                    if maxThungChua.length < thungChua.length
                        maxThungChua = thungChua
                    end
                    thungChua = ""
                end
            end
            if beginIndex == l1
                break
            else
                beginIndex = beginIndex + 1
            end

        end
        return maxThungChua

    end
   
    def self.boDauDauCuoi(street1)
        street = String.new(street1)
        array = [",", "-", ".", "(", ")", " ", "\n"]
        while array.include?street[-1]
            street[-1] = ""
        end
        while array.include?street[0]
            street[0] = ""
        end
        return street.strip
    end
    def self.isNumeric(str1)
        str = str1.gsub(/^0/, "")
        if str.to_i.to_s != str
            return false
        else 
            return true
        end
    end

    def self.splitByLast(strTo, strBe)
        index =  strTo.rindex(strBe) 
        if index != nil 
            if index==0 
                return ["", strTo[index+strBe.size+1..-1]]
            else 
                return [strTo[0..index-1], strTo[index+strBe.size+1..-1]]
            end
        else 
            return [strTo, ""]
        end 
    end

    def self.xoaKyHieu(str)
        dau_r = ['-', ",", "_", "(", ")", ".", "–", "&"]
        dau_r.map { |e|  
            str = str.gsub(e, " ")
        }
        return str
    end 

    def self.compare2String(str1, str2)
        str1_h = createSet(str1)
        str2_h = createSet(str2)

        intersect_set = intersect(str1_h, str2_h)
        union_set = union(str1_h, str2_h)
        dis = calDis(intersect_set, union_set)
        return dis
    end

    def self.createSet(str)
        str = MyString.myDownCase(str)
        str = MyString.xoaKyHieu(str)
        str = MyString.stripString(str)
        str_r = str.split(" ")
        str_h = Hash.new
        str_r.map { |e|  
            if str_h[e]==nil
                str_h[e] = 1
            else 
                str_h[e] = str_h[e] + 1 
            end
        }
        return str_h

    end
    def self.intersect(set1, set2)
        intersect_set = Hash.new
        if set1.size > set2.size 
            setBig = set1
            setSmall = set2
        else
            setBig = set2
            setSmall = set1
        end

        setBig.map { |k,v|  
            if setSmall[k] != nil
                if v >= setSmall[k]
                    intersect_set[k] = setSmall[k]
                else
                    intersect_set[k] = v
                end
            end
        }
        return intersect_set
    end

    def self.union(set1, set2)
        union_set = Hash.new
        if set1.size > set2.size 
            setBig = set1
            setSmall = set2
        else
            setBig = set2
            setSmall = set1
        end
        setBig.map { |k,v|  
            if setSmall[k] != nil
                if v >= setSmall[k]
                    union_set[k] = v
                else
                    union_set[k] = setSmall[k]
                end
            else
                union_set[k] = v
            end
        }
        return union_set
    end

    def self.calDis(intersect_set, union_set)
        union_size = 0
        union_set.map { |k, v|  
            union_size = union_size + union_set[k]
        }
        intersect_size = 0
        intersect_set.map { |k, v|  
            intersect_size = intersect_size + intersect_set[k]
        }
        return intersect_size*1.0/union_size
    end

    def self.isFontError(string)
        errorcharacter_r = ["̀", "́", "̣"] 
        errorcharacter_r.map { |e|  
            if string.include?e
                return true
            end
        }
        return false
    end
end
