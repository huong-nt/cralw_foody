class Crawl
    def self.genNokogiri(link)
        begin
            link = URI.parse(URI.encode(link))
            return Nokogiri::HTML(open(link,'User-Agent'=>'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31').read, nil, 'UTF-8')
            #   return Nokogiri::HTML(open(link))
        rescue
            return false
        end
    end

    def self.genDataInsert(con, hash,table_name)
        data = {
            "fields" => "",
            "datas"  => ""
        }
        hash.each{|key,value|
            data["fields"] = data["fields"] + '`' + key + '`,'
            if value == nil
                value = ""
            end
            data["datas"] = data["datas"] + "'" + con.escape_string(value.to_s) + "',"
        }
        data["fields"] = data["fields"][0..-2]
        data["datas"] = data["datas"][0..-2]
        con.query("INSERT INTO `#{table_name}` (#{data["fields"]}) VALUES (#{data["datas"]})")
    end

    def self.genManyDataInsert(con, array,table_name)
        fields = ""
        if array.size == 0
            return
        end
        array[0].each{|key,value|
            fields = fields + '`' + key + '`,'
        }
        fields[fields.rindex(",")] = " "
        #puts fields
        index = 0
        query = "INSERT INTO `#{table_name}` (#{fields}) VALUES "
        #puts query
        rowsValues = ""
        array.each { |item|
            index = index + 1 
            values = " ("
            item.each{|key, value|
                value = MyString.escStr(value, con)
                values = values + "'#{value}', "
            }
            values[values.rindex(",")] = "), "
            rowsValues = rowsValues + values
            if index == 300
                rowsValues[rowsValues.rindex(",")] = " " 
                con.query (query+rowsValues)
                index = 0 
                query = "INSERT INTO `#{table_name}` (#{fields}) VALUES "
                rowsValues = ""
            end
        }
        if index != 0
            rowsValues[rowsValues.rindex(",")] = " " 
            con.query (query+rowsValues)
        end
    end
   
    def self.createTableByHash(con, table_name,hash)
        query_string = ""
        hash.map { |field_name,field_kind|
            if field_kind.include?'char' or field_kind.include?'text'
                query_string = query_string+"`#{field_name}` #{field_kind} COLLATE utf8_unicode_ci NOT NULL,"+"\n"
            else
                query_string = query_string+"`#{field_name}` #{field_kind} NOT NULL,"+"\n"
            end
        }
        query_string = "CREATE TABLE IF NOT EXISTS `#{table_name}` (
      `id` int(11) NOT NULL AUTO_INCREMENT,"+"\n"+query_string+"PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;"
      con.query(query_string)
    end

    def self.updateState(con, table_name, id)
        con.query("UPDATE `#{table_name}` SET state = 1 WHERE id = #{id}")
    end

    def self.deleteDuplicate(con, tableName,duplicateField)
        res = con.query("SELECT `#{duplicateField}`,count(#{duplicateField}) FROM `#{tableName}` GROUP BY #{duplicateField} HAVING count(#{duplicateField}) > 1")
        while row = res.fetch_row do
            con.query("DELETE FROM `#{tableName}` WHERE `#{duplicateField}` = '#{con.escape_string(row[0])}' LIMIT #{(row[1].to_i-1)}")
        end
    end

    def self.deleteDuplicateFrom2Field(con, tableName,duplicateField, duplicateField2)
        res = con.query("SELECT `#{duplicateField}`, `#{duplicateField2}`, count(#{duplicateField}) FROM `#{tableName}` GROUP BY #{duplicateField}, #{duplicateField2} HAVING count(#{duplicateField}) > 1")
        while row = res.fetch_row do
            con.query("DELETE FROM `#{tableName}` WHERE `#{duplicateField}` = '#{con.escape_string(row[0])}' and `#{duplicateField2}` = '#{con.escape_string(row[1])}' LIMIT #{(row[2].to_i-1)}")
        end
    end


    def self.deg2rad(deg) 
        return deg * (Math::PI/180)
    end

    def self.getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) 
        if lat1 == 0 or lat2 == 0
            return -1 #vo cung lon
        end

        r = 6371 # radius of the earth in km
        dLat = deg2rad(lat2-lat1)  # deg2rad below
        dLon = deg2rad(lon2-lon1) 
        a = 
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
            Math.sin(dLon/2) * Math.sin(dLon/2)

        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)) 
        d = r * c # Distance in km
        #puts "khoang cach: #{d}km"
        return d
    end

    def self.checkLinkImage(link)
    #    puts link
        if link == nil
            return false
        end
        begin
            uri = URI(link)
        rescue
            return false
        end
        begin
        response = Net::HTTP.get_response(uri)

        if response.code=='404'
            # puts "link died"
            return false
        else
            # puts "link ok"
            return true
        end
        rescue
        #   return false;
        end
    end

    def self.getSource(link)
      source = ['amthuc365', 'foody', 'diadiem', 'lukhach24h', 'agoda', 'chudu24', 'ivivu']
      for i in 0..source.size-1 
        e = source[i]
        if link.include?e
          return e
        end
      end
      return ""
    end
    
    def self.getShortNameOfCity(city)
      city = MyString.boDauVaChuThuong(city).gsub(" ", "")
      if city == "thuathienhue"
        city = "hue"
      end
      # if city == "ha_noi"
      #   city = "hanoi"
      # end
      if city == "hochiminh"
        city = "hcm"
      end
      # if city == "quang_nam"
      #   city = "quangnam"
      # end

      #  if city == "quang_ninh"
      #   city = "quangninh"
      # end
      
      return city
    end
end

