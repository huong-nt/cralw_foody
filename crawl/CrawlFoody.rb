# encoding: UTF-8
require_relative "../initialize.rb"
require_relative ROOT_DIRECTORY+"config/DBConfig.rb"
class CrawlFoody
  def initialize(_citynames)
    @city = _citynames
   # @baseLink = _link
    @con = Connector.makeConnect(DBConfig::HOSTNAME, DBConfig::DB_USER, DBConfig::DB_PASS, DBConfig::DB_NAME)
    @source = "crawlfoody"
    @linkTable = "#{@city}_#{@source}_link"
    @detailTable = "#{@city}_#{@source}_detail"
  end

  def createTable
    hash = {
      "link" => "varchar(500)",
      "state" => "varchar(10)",
    }

    Crawl.createTableByHash(@con, @linkTable, hash)

    hash = {
      "name"           => "varchar(500)",
      "address"        => "varchar(500)", 
      "phone"          => "varchar(200)",
      "style"          => "varchar(200)",
      "description"    => "text",
      "vitri"          => "varchar(20)",
      "giaca"          => "varchar(20)",
      "chatluong"      => "varchar(20)",
      "phucvu"         => "varchar(20)",
      "khonggian"      => "varchar(20)",
      "pricerange"     => "varchar(100)",
      "timeopen"       => "varchar(100)",
      "longtitude"     => "varchar(50)",
      "latitude"       => "varchar(50)",
      "images"         => "text"
    }
    Crawl.createTableByHash(@con, @detailTable, hash)
  end

  def getLink
    # Empty table link
    @con.query ("truncate table `#{@linkTable}`") 
  end

  def getDetail
    # Empty table detail
    @con.query ("truncate table `#{@detailTable}`")

      #------ Test key:
      #comment toan bo code phia duoi cua function nay
      #uncomment 2 dong code duoi day
      #link = "http://www.foody.vn/ho-chi-minh/pizza-4p-s"
      #getDetailOfALink(link)
      #----------------------------------------------------------------

    res = @con.query("select * from `#{@linkTable}` where state != '1' ")
    res.each_hash do |row|
      id = row['id']
      link = row['link']
      puts link
      if link == nil
        break
      end
      getDetailOfALink(link)
     @con.query("update `#{@linkTable}` set state = '1' where id = '#{id}' ")
    end
  end

  def getDetailOfALink(link)
    link = link + "/album-anh"
    doc = Crawl.genNokogiri(link)
    hash = Hash.new
    hash['name'] = getName(doc)
    hash['address'] = getAddress(doc)
    hash['phone'] = getPhone(doc)
    hash['style'] = getStyle(doc)
    hash['description'] = getDescription(doc)
    hash['vitri']= getVitri(doc)
    hash['giaca']= getGiaca(doc)
    hash['chatluong']= getChatluong(doc)
    hash['phucvu']= getPhucvu(doc)
    hash['khonggian']= getKhonggian(doc)
    hash['pricerange']= getPricerange(doc)
    hash['timeopen']= getTimeopen(doc)
    hash['images']= getImages(doc)
    hash['latitude'] = getLatitude(doc)
    hash['longtitude'] = getLongtitude(doc)
    Crawl.genDataInsert(@con, hash, @detailTable)
  end

  def getName(doc)
    name =  doc.css("meta[property='og:title']").attr("content")
    return name
  end
  def getAddress(doc)
    address =  doc.css("div[class='res-common-add']")
    address_s = address.css("span")[0].inner_text + address.css("span")[1].css("a").inner_text + ", " + address.css("span")[2].inner_text
    return MyString.stripString(address_s)
  end
  def getPhone(doc)
    phone = doc.css("span[itemprop='telephone']").inner_text
    return MyString.stripString(phone)
  end
  def getStyle(doc)
    style = ""
    doc.css("a[class='microsite-cuisine']").map{|item|
     style = style + item.inner_text
    }
    puts style
    return MyString.stripString(style)
  end
  def getDescription(doc)
    description = doc.css("meta[name='description']").attr("content")
    return description
  end
  def getVitri(doc)
    p = doc.css("div[class='microsite-top-points']")[0].css("span").inner_text
    return MyString.stripString(p)  
  end
  def getGiaca(doc)
    p = doc.css("div[class='microsite-top-points']")[1].css("span").inner_text
    return MyString.stripString(p)  
  end
  def getChatluong(doc)
    p = doc.css("div[class='microsite-top-points']")[2].css("span").inner_text
    return MyString.stripString(p)  
  end
  def getPhucvu(doc)
    p = doc.css("div[class='microsite-top-points']")[3].css("span").inner_text
    return MyString.stripString(p)  
  end
  def getKhonggian(doc)
    p = doc.css("div[class='microsite-top-points']")[4].css("span").inner_text
    return MyString.stripString(p)  
  end
  def getPricerange(doc)
    pricerange = doc.css("span[itemprop='priceRange']").inner_text
    return MyString.stripString(pricerange)  
  end
  def getTimeopen(doc)
    time = doc.css("div[class='res-common-price']").css("span")[0].css("span")[0].inner_text
    return MyString.stripString(time)  
  end
  def getLatitude(doc)
    lat = doc.css("meta[property='place:location:latitude']").attr("content")
    return lat  
  end
  def getLongtitude(doc)
    lng = doc.css("meta[property='place:location:longitude']").attr("content")
    return lng 
  end
  def getImages(doc)
    image = ""
    doc.css("div[class='micro-home-album-img']").map{|item|

     image = image + item.css("a").attr("href") + " |"
    }
    return MyString.stripString(image)
  end
end